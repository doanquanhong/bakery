<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
	<link rel="stylesheet" href="css/loginStyle.css">
</head>
<body>
	<?php
		//Gọi file connection.php ở bài trước
		//require_once("lib/connection.php");
		// Kiểm tra nếu người dùng đã ân nút đăng nhập thì mới xử lý
		if (isset($_POST["LoginSubmit"])) {
			// lấy thông tin người dùng
			$username = $_POST["UsernameLogin"];
			$password = $_POST["PasswordLogin"];
			//làm sạch thông tin, xóa bỏ các tag html, ký tự đặc biệt 
			//mà người dùng cố tình thêm vào để tấn công theo phương thức sql injection
			//$username = strip_tags($username);
			//$username = addslashes($username);
			//$password = strip_tags($password);
			//$password = addslashes($password);
			if ($username == "" || $password =="") {
				echo "username hoặc password bạn không được để trống!";
			}else{
				//$sql = "select * from users where username = '$username' and password = '$password' ";
				//$query = mysqli_query($conn,$sql);
				//$num_rows = mysqli_num_rows($query);
				//if ($num_rows==0) {
					//echo "tên đăng nhập hoặc mật khẩu không đúng !";
				//}else{
					//tiến hành lưu tên đăng nhập vào session để tiện xử lý sau này
					//$_SESSION['UsernameLogin'] = $username;
					// Thực thi hành động sau khi lưu thông tin vào session
					// ở đây mình tiến hành chuyển hướng trang web tới một trang gọi là index.php
					header('Location: index.php');
				}
			}
		//}
	?>
	<form  class="box" action="login.php" method="POST">

		<h1>Login</h1>
		<input type="text" name="UsernameLogin"placeholder="Username">
		<input type="password" name="PasswordLogin" placeholder="Password">
		<input type="submit" name="LoginSubmit" value="Login">		
		
		<h2 id="Forget_Username_Password__">
			<span>Forget Username/Password?</span><br>
		</h2>
	
	</form>
</body>
</html>